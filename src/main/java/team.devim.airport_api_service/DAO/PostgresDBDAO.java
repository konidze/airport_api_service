package team.devim.airport_api_service.DAO;

import org.apache.log4j.Logger;
import team.devim.airport_api_service.Pojo.AirportStatistic;
import team.devim.airport_api_service.Pojo.PassengerStatistic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PostgresDBDAO implements StatisticDAO {
    static Connection connection;
    private static final Logger log = Logger.getLogger(PostgresDBDAO.class);
    static {
        try {
            log.info("Connection");
            connection = DataSource.getConnection();
        } catch (SQLException e) {
            log.error(e.getMessage());
        }
    }
    @Override
    public PassengerStatistic getPassengerStatistic(String ticketNo) {
        PassengerStatistic passengerStatistic = null;
        String queryPersonal = "select bt.ticket_no, bt.passenger_name, tf.fare_conditions, fl.status, fl.departure_airport   " +
                " from  bookings.tickets as bt " +
                " inner join bookings.ticket_flights tf  on bt.ticket_no  = tf.ticket_no" +
                " inner join bookings.flights fl on tf.flight_id=fl.flight_id    " +
                "  inner join bookings.flights_v fv on fl.scheduled_departure = fv.scheduled_departure " +
                " where bt.ticket_no = ? order by fl.scheduled_departure asc limit 1";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(queryPersonal);
            preparedStatement.setString(1, ticketNo);
            ResultSet result = preparedStatement.executeQuery();
            passengerStatistic = displayPassengerStatistic(result);
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(ex.getMessage());
        }
        return passengerStatistic;
    }

    PassengerStatistic displayPassengerStatistic(ResultSet result) throws SQLException {
        PassengerStatistic passengerStatistic = null;
        while (result.next()) {
            passengerStatistic = new PassengerStatistic("passenger_name", "fare_conditions", "status", "departure_airport");
            passengerStatistic.passengerName = result.getString("passenger_name");
            passengerStatistic.fareConditions = result.getString("fare_conditions");
            passengerStatistic.statusFlight = result.getString("status");
            passengerStatistic.departureAirport = result.getString("departure_airport");
        }
        return passengerStatistic;
    }

    @Override
    public AirportStatistic getAirportStatistic(String airportCo) {
        AirportStatistic airportStatistic = null;
        String queryAirport = "select count(distinct f.flight_id) as count_flights, avg(tf.amount) as avg_amount," +
                " count(bp.boarding_no) as count_passengers" +
                " from bookings.flights as f" +
                " inner join bookings.ticket_flights tf  on f.flight_id = tf.flight_id" +
                " inner join bookings.boarding_passes bp on f.flight_id = bp.flight_id" +
                " where f.departure_airport = ? ";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(queryAirport);
            preparedStatement.setString(1, airportCo);
            ResultSet result = preparedStatement.executeQuery();
            airportStatistic = displayAirportStatistic(result);
        } catch (SQLException ex) {
            ex.printStackTrace();
            log.error(ex.getMessage());
        }
        return airportStatistic;
    }

    AirportStatistic displayAirportStatistic(ResultSet result) throws SQLException {
        AirportStatistic airportStatistic = null;
        while (result.next()) {
            airportStatistic = new AirportStatistic("count_flights", "avg_amount", "count_passengers");
            airportStatistic.countFlights = result.getString("count_flights");
            airportStatistic.avgAmount = result.getString("avg_amount");
            airportStatistic.countPassengers = result.getString("count_passengers");

        }
        return airportStatistic;
    }
}
