package team.devim.airport_api_service.DAO;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import team.devim.airport_api_service.Constants;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {
    private static final HikariConfig HIKARI_CONFIG = new HikariConfig();
    private static final HikariDataSource HIKARI_DATA_SOURCE;

    static {
        HIKARI_CONFIG.setJdbcUrl(Constants.POSTGRES_DATA_URL);
        HIKARI_CONFIG.setUsername(Constants.POSTGRES_DATA_USERNAME);
        HIKARI_CONFIG.setPassword(Constants.POSTGRES_DATA_PASSWORD);
        HIKARI_CONFIG.addDataSourceProperty("cachePrepStmts", "true");
        HIKARI_CONFIG.addDataSourceProperty("prepStmtCacheSize", "250");
        HIKARI_CONFIG.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
        HIKARI_CONFIG.setMaximumPoolSize(5);
        HIKARI_DATA_SOURCE = new HikariDataSource(HIKARI_CONFIG);
    }

    private DataSource() {
    }

    public static Connection getConnection() throws SQLException {
        return HIKARI_DATA_SOURCE.getConnection();
    }
}
