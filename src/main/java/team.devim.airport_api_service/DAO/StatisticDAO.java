package team.devim.airport_api_service.DAO;


import team.devim.airport_api_service.Pojo.AirportStatistic;
import team.devim.airport_api_service.Pojo.PassengerStatistic;

public interface StatisticDAO {
    PassengerStatistic getPassengerStatistic(String ticketNo);

    AirportStatistic getAirportStatistic(String airportCo);
}
