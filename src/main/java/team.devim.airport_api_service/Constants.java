package team.devim.airport_api_service;

public class Constants {
    public static final String POSTGRES_DATA_URL = System.getenv("db_airport_url");
    public static final String POSTGRES_DATA_USERNAME = System.getenv("db_airport_userName");
    public static final String POSTGRES_DATA_PASSWORD = System.getenv("db_airport_password");
    public static final String REDIS_HOST = System.getenv("redis_host");
    public static final boolean HAVE_REDIS = true;
}
