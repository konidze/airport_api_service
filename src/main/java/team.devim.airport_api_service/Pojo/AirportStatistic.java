package team.devim.airport_api_service.Pojo;

public class AirportStatistic {
    public String countFlights;
    public String avgAmount;
    public String countPassengers;

    public AirportStatistic(String countFlights, String avgAmount, String countPassengers) {
        this.countFlights = countFlights;
        this.avgAmount = avgAmount;
        this.countPassengers = countPassengers;
    }

    @Override
    public String toString() {
        return "AirportStatistic{" +
                "countFlights='" + countFlights + '\'' +
                ", avgAmount='" + avgAmount + '\'' +
                ", countPassengers='" + countPassengers + '\'' +
                '}';
    }
}
