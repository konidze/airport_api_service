package team.devim.airport_api_service.Pojo;

public class PassengerStatistic {
    public String passengerName;
    public String fareConditions;
    public String statusFlight;
    public String departureAirport;

    public PassengerStatistic(String passengerName, String fareConditions, String statusFlight, String departureAirport) {
        this.passengerName = passengerName;
        this.fareConditions = fareConditions;
        this.statusFlight = statusFlight;
        this.departureAirport = departureAirport;
    }

    @Override
    public String toString() {
        return "PassengerStatistic{" +
                "passengerName='" + passengerName + '\'' +
                ", fareConditions='" + fareConditions + '\'' +
                ", statusFlight='" + statusFlight + '\'' +
                ", departureAirport='" + departureAirport + '\'' +
                '}';
    }
}
