package team.devim.airport_api_service;

import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import team.devim.airport_api_service.DAO.PostgresDBDAO;
import team.devim.airport_api_service.Pojo.AirportStatistic;
import team.devim.airport_api_service.Pojo.PassengerStatistic;


import java.util.logging.Logger;

import static spark.Spark.get;
import static team.devim.airport_api_service.Constants.HAVE_REDIS;
import static team.devim.airport_api_service.Constants.REDIS_HOST;


public class Main {
    static final Logger log = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        PostgresDBDAO postgresDBDAO = new PostgresDBDAO();
        if (HAVE_REDIS) {
            try (Jedis jedis = new Jedis(REDIS_HOST)) {
                log.info("Connection to redis");
                get("/tickets", (req, res) -> {
                    String ticketNo = req.queryParams("ticket_no");
                    if (!jedis.exists(ticketNo)) {
                        log.info("Not exist. Existing.");
                        PassengerStatistic passengerStatistic = postgresDBDAO.getPassengerStatistic(ticketNo);
                        Gson gson = new Gson();
                        String passenger = gson.toJson(passengerStatistic, PassengerStatistic.class);
                        jedis.set(ticketNo, passenger);
                        return passenger;
                    } else {
                        log.info("Exist.");
                        return jedis.get(ticketNo);
                    }
                });

                get("/airport", (req, res) -> {
                    String airportCo = req.queryParams("airport_code");
                    if (!jedis.exists(airportCo)) {
                        log.info("Not exist. Existing.");
                        AirportStatistic airportStatistic = postgresDBDAO.getAirportStatistic(airportCo);
                        Gson gsonAirport = new Gson();
                        String airport = gsonAirport.toJson(airportStatistic, AirportStatistic.class);
                        return airport;
                    } else {
                        log.info("Exist.");
                        return jedis.get(airportCo);
                    }
                });
            }
        } else {
            getPostgreRequest();
        }
    }

    public static void getPostgreRequest() {
        PostgresDBDAO postgresDBDAO = new PostgresDBDAO();
        get("/tickets", (req, res) -> {
            String ticketNo = req.queryParams("ticket_no");
            PassengerStatistic passengerStatistic = postgresDBDAO.getPassengerStatistic(ticketNo);
            Gson gson = new Gson();
            String passenger = gson.toJson(passengerStatistic, PassengerStatistic.class);
            return passenger;
        });

        get("/airport", (req, res) -> {
            String airportCo = req.queryParams("airport_code");
            log.info("Not exist. Existing.");
            AirportStatistic airportStatistic = postgresDBDAO.getAirportStatistic(airportCo);
            Gson gsonAirport = new Gson();
            String airport = gsonAirport.toJson(airportStatistic, AirportStatistic.class);
            return airport;
        });
    }
}
